from nltk.corpus import stopwords
from tkinter import Tk
from tkinter.filedialog import askopenfilename
import pandas as pd
from wordcloud import WordCloud
import matplotlib.pyplot as plt
import numpy as np
import os
from os import path
from PIL import Image

indicador = input("Digite o indicador que deseja gerar a nuvem de palavras: ")

Tk().withdraw()
file = askopenfilename()
df = pd.read_excel(file, sheet_name="propriedade")
indicadores = df[indicador]
#Palavras que deseja remover da nuvem de palavras
dict_words = set(stopwords.words(f"{os.getenv('stopwords_path')}/stopwords.txt"))
d = path.dirname(__file__) if "__file__" in locals() else os.getcwd()
# Imagem vetorizada
mask_cloud = np.array(Image.open(path.join(d, 'cloud.png')))

comunidades = []

for c in df['1.1.4']:
    if c not in comunidades:
        comunidades.append(c)

for comunidade in comunidades:

    loc1 = indicadores.loc[(df['1.1.4'] == comunidade)]

    if comunidade == 'Furquim':
        cloud = WordCloud(contour_width=3, max_words=1000, font_path='verdana', background_color="white",
                          stopwords=dict_words, width=700, height=500, margin=1,
                          color_func=lambda *args, **kwargs: (47, 49, 146), mask=mask_cloud, min_font_size=15).generate(
            str(loc1))
        plt.imshow(cloud)
        plt.axis('off')
        plt.savefig(f"cloud_{indicador}-{comunidade}.png")
    elif comunidade == 'Águas Claras':
        cloud = WordCloud(contour_width=3, max_words=1000, font_path='verdana', background_color="white",
                          stopwords=dict_words, width=700, height=500, margin=1,
                          color_func=lambda *args, **kwargs: (0, 168, 142), min_font_size=15).generate(str(loc1))
        plt.imshow(cloud)
        plt.axis('off')
        plt.savefig('cloud_3.3.24-' + comunidade + '.png')
    elif comunidade == 'Monsenhor Horta':
        cloud = WordCloud(contour_width=3, max_words=1000, font_path='verdana', background_color="white",
                          stopwords=dict_words, width=700, height=500, margin=1,
                          color_func=lambda *args, **kwargs: (242, 102, 34), min_font_size=15).generate(str(loc1))
        plt.imshow(cloud)
        plt.axis('off')
        plt.savefig(f"cloud_{indicador}-{comunidade}.png")
    elif comunidade == 'Antônio Pereira':
        cloud = WordCloud(contour_width=3, max_words=1000, font_path='verdana', background_color="white",
                          stopwords=dict_words, width=700, height=500, margin=1,
                          color_func=lambda *args, **kwargs: (242, 102, 34), min_font_size=15).generate(str(loc1))
        plt.imshow(cloud)
        plt.axis('off')
        plt.savefig(f"cloud_{indicador}-{comunidade}.png")
    elif comunidade == 'Santa Rita Durão':
        cloud = WordCloud(contour_width=3, max_words=1000, font_path='verdana', background_color="white",
                          stopwords=dict_words, width=700, height=500, margin=1,
                          color_func=lambda *args, **kwargs: (0, 168, 142), min_font_size=15).generate(str(loc1))
        plt.imshow(cloud)
        plt.axis('off')
        plt.savefig(f"cloud_{indicador}-{comunidade}.png")
    elif comunidade == 'Camargos':
        cloud = WordCloud(contour_width=3, max_words=1000, font_path='verdana', background_color="white",
                          stopwords=dict_words, width=700, height=500, margin=1,
                          color_func=lambda *args, **kwargs: (47, 49, 146), min_font_size=15).generate(str(loc1))
        plt.imshow(cloud)
        plt.axis('off')
        plt.savefig(f"cloud_{indicador}-{comunidade}.png")
    elif comunidade == 'Propriedades Rurais':
        cloud = WordCloud(contour_width=3, max_words=1000, font_path='verdana', background_color="white",
                          stopwords=dict_words, width=700, height=500, margin=1,
                          color_func=lambda *args, **kwargs: (0, 130, 200), min_font_size=15).generate(str(loc1))
        plt.imshow(cloud)
        plt.axis('off')
        plt.savefig(f"cloud_{indicador}-{comunidade}.png")
    elif comunidade == 'Paracatu de Baixo':
        cloud = WordCloud(contour_width=3, max_words=1000, font_path='verdana', background_color="white",
                          stopwords=dict_words, width=700, height=500, margin=1,
                          color_func=lambda *args, **kwargs: (232, 132, 107), min_font_size=15).generate(str(loc1))
        plt.imshow(cloud)
        plt.axis('off')
        plt.savefig(f"cloud_{indicador}-{comunidade}.png")

print('Concluído')


