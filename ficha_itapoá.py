import pandas as pd
from tkinter import Tk
from tkinter.filedialog import askopenfilename
import openpyxl


#Seleciona a base e cria os DataFrames
Tk().withdraw()
file = askopenfilename()
df = pd.read_excel(file, sheet_name = "pessoas")
df_domicilio = pd.read_excel(file, sheet_name = "propriedade")
df_idf = pd.read_excel(file, sheet_name = "IDF")
df_domicilio2 = pd.read_excel(file, sheet_name = "propriedade2")
info_familia = df[["Indexador","Composição Familiar","Nome","Idade","Estado Civil","Possui alguma deficiência?","Possui alguma doença crônica?","Gestante ou Lactante?"]]
info_familia2 = df[["Indexador","Composição Familiar", "Nome", "Frequenta a Escola?",  "Qual o último grau ou série frequentado com aprovação?", "Situação Ocupacional"]]
info_familia3 = df[["Indexador", "Composição Familiar", "Nome", "Possui Beneficio social",  "RF", "RFPC", "RFPC (SM)"]]
idf_familia = df_idf[["Indexador", "Vulnerabilidade", "Acesso ao Conhecimento", "Acesso ao Trabalho", "Disponibilidade de Recursos", "Desenvolvimento Infantil", "Condições Habitacionais", "IDF"]]
info_domicilio = df_domicilio[["Indexador", "Material Predominante", "Uso e Ocupação do Imóvel", "Número de Cômodos", "Número de Dormitórios", "Número de habitantes por dormitório", "Titularidade do Terreno"]]
info_domicilio2 = df_domicilio[["Indexador", "Documento do Terreno", "Tempo de Moradia na Comunidade (Em anos)", "Abastecimento de Água", "Qual era a fonte principal de energia elétrica na edificação?", "Esgotamento Sanitário", "Coleta de Lixo"]]

#Cria uma lista de PrimaryKey

y = []
for indexador in df['Indexador']:
    if indexador not in y:
        y.append(indexador)
# s = []
# for status in df_domicilio["Status da pesquisa"]:
#         if status not in s:
#                 s.append(status)

#Usa a pk como parametro na função df.loc
for h in y:
#Caracteristicas do html. Obs: Pode-se aplicar CSS e JavaScript direto na body.
        HEADER = '''
        <html>
        <head>
                <style>
                        table{
                        width: 100%;
                        border-collapse: collapse;
                        }
                        table thead{
                        background: #9C9C9C;  
                        }
                        table{
                        border: transparent;
                        border-bottom-color:black;
                        }
                        td {
                        text-align: center;
                        }
                        
                        tr {
                        text-align: center;
                        }
                        
                        th {
                        text-align: center;
                        background: #9C9C9C;
                        }
                        #renda{
                        background:#F5F5DC;
                        }
                        .col-md-0{
                        background: #9C9C9C;
                        height:30px;
                        text-align: center;
                        vertical-align: middle;
                        line-height: 30px; 
                        }
                        .col-md-1{
                        background: #DAA520;
                        height:30px;
                        color:white;
                        text-align: center;
                        vertical-align: middle;
                        line-height: 30px;
                        
                        }
                        
                </style> 
                <div class='col-md-0'><b>SÍNTESE SOCIOECONOMICA DA FAMÍLIA RESIDENTE</b></div>
        <b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspIndexador:</b>'''+h+'''</b>'''''' 
                <div class='col-md-1'><b><p><center>INFORMAÇÕES DA FAMÍLIA</b></p></center></div>               
        </head>
        <body>
        
        
        
        '''''''''
        HEADER2 = '''''''''
        <html>
        <div class='col-md-1'><b><p><center>INFORMAÇÕES DO DOMICÍLIO</b></p></center></div>
        <head>
                
        </head>
        <body>
        '''''
        # HEADER4 = ''''
        # <html>
        #         <h2>Informações da moradia atual
        #         <br>
        # <head>
                
        # </head>
        # <body>
        # '''
        FOOTER = '''
        </body>
        </html>
        '''
        loc1 = info_familia.loc[(df['Indexador'] == h)]
        loc2 = info_familia2.loc[(df['Indexador'] == h)]
        loc3 = info_familia3.loc[(df['Indexador'] == h)]
        loc_idf = idf_familia.loc[(df_idf['Indexador'] == h)]
        loc4 = info_domicilio.loc[(df_domicilio['Indexador'] == h)]
        loc5 = info_domicilio2.loc[(df_domicilio['Indexador'] == h)]
      
        
#Cria os html com os campos definidos pelos df's
        with open(h +'.html', 'w', encoding="utf-8") as f:
                f.write(HEADER)
                f.write(loc1.head().to_html(columns=["Nome", "Composição Familiar","Idade","Estado Civil","Possui alguma deficiência?","Possui alguma doença crônica?","Gestante ou Lactante?"], index=False, classes='df', justify='center'))
                f.write(loc2.head().to_html(columns=["Nome", "Frequenta a Escola?",  "Qual o último grau ou série frequentado com aprovação?", "Situação Ocupacional"],index=False, classes='df', justify='center'))
                f.write(loc3.head().to_html(columns=["Nome",  "Possui Beneficio social",  "RF", "RFPC", "RFPC (SM)"], index=False, classes='df', justify='center'))
                f.write(loc_idf.head().to_html(columns=["Vulnerabilidade", "Acesso ao Conhecimento", "Acesso ao Trabalho", "Disponibilidade de Recursos", "Desenvolvimento Infantil", "Condições Habitacionais", "IDF"],index=False, classes='df', justify='center'))            
                f.write(FOOTER)
                f.write(HEADER2)
                f.write(loc4.head().to_html(columns=["Material Predominante", "Uso e Ocupação do Imóvel", "Número de Cômodos", "Número de Dormitórios", "Número de habitantes por dormitório", "Titularidade do Terreno"], index=False, classes='df', justify='center'))
                f.write(loc5.head().to_html(columns=["Documento do Terreno", "Tempo de Moradia na Comunidade (Em anos)", "Abastecimento de Água", "Qual era a fonte principal de energia elétrica na edificação?", "Esgotamento Sanitário", "Coleta de Lixo"], index=False, classes='df', justify='center'))
                f.write(FOOTER)
print(loc3)
