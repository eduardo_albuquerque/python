import pandas as pd
from tkinter import Tk
from tkinter.filedialog import askopenfilename
import openpyxl


#Seleciona a base e cria os DataFrames
Tk().withdraw()
file = askopenfilename()
df = pd.read_excel(file, sheet_name = "GERAL")
df1 = pd.read_excel(file, sheet_name = "Planilha1")
info_familia = df[["Indexador", "Composição Familiar (Nome Completo)","Grau de Parentesco com o(a) responsável", "RG", "CPF", "Sexo","Idade", "Data de Nascimento", "Classificação", "Situação Ocupacional", "Telefone do(a) Responsável"]]
info_atendimento = df[["Indexador", "Composição Familiar (Nome Completo)", "Frequenta Escola","Ano Escolar","Grau de Escolaridade", "Possui Deficiência", "Gestante"]]
info_moradia_origem = df1[["Indexador", "Endereço - Origem","Bairro - Origem","Município - Origem"]]
info_moradia_atual = df1[["Indexador", "Endereço - Atual","Bairro - Atual","Município - Atual", "Elegibilidade"]]

#Cria uma lista de PrimaryKey

y = []
for Indexador in df['Indexador']:

    if Indexador not in y:
        y.append(Indexador)
r = []
for responsável in df['Responsável Pela Família']:
        if responsável not in r:
                r.append(responsável)

#Usa a pk como parametro na função df.loc
for h in y:
#Caracteristicas do html. Obs: Pode-se aplicar CSS e JavaScript direto na body.
        HEADER = '''
        <html>
        <head>
                <style>
                
                        @media (orientation: landscape) {
                                table{
                        width: 100%;
                        border-collapse: collapse;
                        }
                        table thead{
                        background: #9C9C9C;  
                        }
                        table{
                        border: transparent;
                        border-bottom-color:black;
                        }
                        td {
                        text-align: center;
                        }
                        
                        tr {
                        text-align: center;
                        }
                        
                        th {
                        text-align: center;
                        background: #9C9C9C;
                        # }
                        #renda{
                        background:#F5F5DC;
                        }
                        .col-md-0{
                        background: #9C9C9C;
                        height:30px;
                        text-align: center;
                        vertical-align: middle;
                        line-height: 30px; 
                        }
                        .col-md-1{
                        background: #DAA520;
                        height:30px;
                        color:white;
                        text-align: left;
                        font-size: 15px;
                        vertical-align: middle;
                        line-height: 30px;

                        .col-md-1 h2{
                        text-align: center;
                        }

                        }       

                        
                </style>                
        </head>
        <body>
        <div class="col-md-0"><h2>Ficha Famíliar</h2></div>
        <div class="col-md-1">Indexador da pesquisa - '''+ h +'''</div>
        
        '''
        HEADER2 = '''
                <div class="col-md-1"><center><h2>Características da Família</h2></div></center>
        '''
        HEADER3 = '''
                <div class="col-md-1"><center><h2>Informações da moradia origem</h2></div></center>
        '''
        HEADER4 = '''
        <div class="col-md-1"><center><h2>Informações da moradia atual</h2></div></center>
        '''
        FOOTER = '''
        </body>
        </html>
        '''
        loc1 = info_familia.loc[(df['Indexador'] == h)]
        loc2 = info_atendimento.loc[(df['Indexador'] == h)]
        loc3 = info_moradia_origem.loc[(df1['Indexador'] == h)]
        loc4 = info_moradia_atual.loc[(df1['Indexador'] == h)]


#Cria os html com os campos definidos pelos df's
        with open(h + 'html', 'w', encoding="utf-8") as f:
                f.write(HEADER)
                f.write(loc1.to_html(columns=["Composição Familiar (Nome Completo)","Grau de Parentesco com o(a) responsável", "RG", "CPF", "Sexo","Idade", "Data de Nascimento", "Classificação", "Situação Ocupacional", "Telefone do(a) Responsável"], classes='df', index=False, na_rep='missing', justify='center'))
                f.write(HEADER2) 
                f.write(loc2.head().to_html(columns=["Composição Familiar (Nome Completo)","Frequenta Escola", "Frequenta Escola","Ano Escolar","Grau de Escolaridade", "Possui Deficiência", "Gestante"], classes='df', index=False, na_rep='missing', justify='center'))
                f.write(HEADER3)
                f.write(loc3.head().to_html(columns=["Endereço - Origem","Bairro - Origem","Município - Origem"], classes='df', index=False, na_rep='missing', justify='center'))
                f.write(HEADER4)
                f.write(loc4.head().to_html(columns=["Endereço - Atual","Bairro - Atual","Município - Atual", "Elegibilidade"], classes='df', index=False, na_rep='missing', justify='center'))
                f.write(FOOTER)

print("Concluído")



