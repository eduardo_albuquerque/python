import shapefile
from tkinter import Tk
from tkinter.filedialog import askopenfilename
import pandas as pd
import openpyxl
import os
import xlsxwriter

# path = "Path"
# dir = os.listdir(path)
# Solicita o arquivo ao usuário
Tk().withdraw()
file = askopenfilename()
# lê o arquivo
sf = shapefile.Reader(file, encoding="latin1")
# seleciona os campos e os registros
fields = [x[0] for x in sf.fields][1:]
records = sf.records()
shps = [s.points for s in sf.shapes()]
# cria o dataframe
df = pd.DataFrame(columns=fields, data=records)
df = df.assign(coords=shps)
#df['coords'] = ('{} \n '.format(df['coords']))
# cria e salva um arquivo xlsx com o dataframe
print(df)
coords = df['coords']
coords2 = []
for x in coords:
    # ordered = [x[::-1] for x in coords]
    coords2.append([y[::-1] for y in x])
# print(coords2)
df_coord = pd.DataFrame(coords2)

print(df_coord)

# for file in os:
#     if file == 'shpfile.xlsx':
#         os.remove(file)
#     else:
#         writer = pd.ExcelWriter('shpfile.xlsx', engine ='xlsxwriter')


writer = pd.ExcelWriter('shpfile.xlsx', engine='xlsxwriter')

df_coord.to_excel(writer, sheet_name='Planilha1')
writer.save()
